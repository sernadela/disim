package pt.ua.bioinformatics.disim.connect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * SQL database connection layer.
 *
 * @author sernadela
 */
public class DB {

    private Connection connection;
    private Statement statement;
    private PreparedStatement pStatement;
    private String connectionString;
    private String database;

    public String getConnectionString() {
        return connectionString;
    }

    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    /**
     * Constructor with custom database information.
     *
     * @param database The database name
     * @param connectionString The connection string (if distinct from WAVe)
     */
    public DB(String database, String connectionString) {
        this.database = database;
        this.connectionString = connectionString;
    }
    
    /**
     * Establishes a new connection to the database.
     * <p><b>Feature:</b><br />
     * Performs the required operations to establish a new database connection
     * to non-default database
     * </p>
     *
     * @return Success of the operation (true if connects, false if fails to
     * connect)
     */
    public boolean connect() {
        boolean success = false;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(connectionString);
            statement = connection.createStatement();
            success = true;
        } catch (SQLException e) {
            System.out.println("[DB] Unable to connect to " + database + "\n\t" + e.toString());
            success = false;
        } catch (ClassNotFoundException ex) {
            System.out.println("[DB] Unable to connect to " + database + "\n\t" + ex.toString());
            success = false;
        } 
        return success;
    }

    /**
     * Closes current database connection.
     * <p><b>Feature:</b><br />
     * Performs the required operations to close an open database connection
     * </p>
     *
     * @return Success of the operation (true if it closes the connection, false
     * if it fails to close the connection)
     */
    public boolean close() {
        try {
            if (!statement.isClosed()) {
                statement.close();
            }
            if (!pStatement.isClosed()) {
                pStatement.close();
            }
            if (!connection.isClosed()) {
                connection.close();
            }

        } catch (SQLException e) {
            System.out.println("[DB] Unable to close " + database + " connection\n\t" + e.toString());
            return false;
        } 
        return true;

    }


    /**
     * Retrieves data from the instance database.
     * <p><b>Feature:</b><br/>
     * Retrieves a Result Set table containing the results from the provided
     * <code>SELECT</code> statement
     *
     * @param query <code>SELECT</code> query to get the desired data
     * @return Result Set containing the output of the
     * executed <code>SELECT</code> statement
     */
    public ResultSet getData(String query) {

        ResultSet rs = null;

        try {
            if (!connection.isClosed()) {
                pStatement=connection.prepareStatement(query);
                rs = pStatement.executeQuery();
            }
        } catch (SQLException e) {
            System.out.println("[DB] Unable to retrieve data\n\t" + e.toString());
            return rs;
        } 
        return rs;

    }

}
