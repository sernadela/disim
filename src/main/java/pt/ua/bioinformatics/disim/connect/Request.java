/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ua.bioinformatics.disim.connect;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;
import org.apache.log4j.Logger;
import pt.ua.bioinformatics.disim.Main;
import pt.ua.bioinformatics.disim.domain.IndividualSample;

/**
 * Class to make requests
 *
 * @author sernadela
 */
public class Request {
    
    private static final Logger logger = Logger.getLogger(Request.class.getName());

    /**
     * Make the requests based on the the Individual Type
     *
     * @param samples
     * @return
     */
    public List<IndividualSample> requestTimes(List<IndividualSample> samples) {
        for (IndividualSample sample : samples) {

            if (sample.getType().equalsIgnoreCase("SQL")) {
                sample = requestDBTimes(sample);
            } else {
                sample = requestFileTimes(sample);
            }

        }
        return samples;
    }

    /**
     * Fill with real File request times
     *
     * @param sample
     * @return
     */
    private IndividualSample requestFileTimes(IndividualSample sample) {
        for (String data : sample.getDataTimes().keySet()) {
            try {
                URL u = new URL(sample.getConnection().replace(IndividualSample.REPLACER, data));
                long i = System.currentTimeMillis();
                InputStream is = u.openStream();
                long f = System.currentTimeMillis();
                int bytes=is.available();
                sample.getStorage().put(data, (long)bytes);
                sample.getDataTimes().put(data, f - i);
                
                

                logger.debug(u.toString() + "\n FILE REQUEST done in " + ((f - i)) + " miliseconds. Expected storage (bytes):"+bytes+" \n");
            } catch (MalformedURLException ex) {
                logger.error(ex);
            } catch (IOException ex) {
                logger.error(ex); 
            }
        }
        return sample;
    }

    /**
     * Fill with real DB request times
     *
     * @param sample
     * @return
     */
    private IndividualSample requestDBTimes(IndividualSample sample) {

        DB db = new DB("DB Test for " + sample.getDescription(), sample.getConnection());
        logger.debug("\n DB CREATING CONNECTION for " + sample.getConnection());
        db.connect();
        for (String data : sample.getDataTimes().keySet()) {

            String query = sample.getQuery().replace(IndividualSample.REPLACER, data);

            //jdbc:mysql://mysql-amigo.ebi.ac.uk:4085/go_latest?user=go_select&password=amigo
            logger.debug("\n DB SUBMIT QUERY: " + query);
            long i = System.currentTimeMillis();
            ResultSet rs = db.getData(query);
            long f = System.currentTimeMillis();

            int numRows = 0;
            int rsSize = 0;
            int rsTotalSize = 0;
            try {
                while (rs.next()) {
                    numRows++;
                }
                ResultSetMetaData rsmd = rs.getMetaData();
                int columnCount = rsmd.getColumnCount();
                // The column count starts from 1
                for (int m = 1; m < columnCount + 1; m++) {
                    int precision = rsmd.getPrecision(m);
                    rsSize = rsSize + precision;
                }
                rsTotalSize=rsSize * numRows;
            } catch (SQLException ex) {
                //ignore
            }
            sample.getStorage().put(data, (long)rsTotalSize);
            sample.getDataTimes().put(data, f - i);
            logger.debug(" DB QUERY done in " + ((f - i)) + " miliseconds. Expected storage (bytes): " + rsTotalSize+" \n");

        }
        db.close();
        return sample;
    }

}
