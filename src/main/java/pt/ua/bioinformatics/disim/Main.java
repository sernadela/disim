package pt.ua.bioinformatics.disim;

import pt.ua.bioinformatics.disim.domain.IndividualSample;
import pt.ua.bioinformatics.disim.model.OntologyLoader;
import pt.ua.bioinformatics.disim.model.SimModel;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;
import pt.ua.bioinformatics.disim.connect.Request;
import pt.ua.bioinformatics.disim.stats.Statistic;

/**
 *
 * @author Pedro Sernadela <sernadela at ua.pt>
 */
public class Main {

    private static final Logger logger = Logger.getLogger(Main.class.getName());
    private static final int SIMULATIONS_NUMBER=5;

    public static void main(String[] args) {

        //read simulation ontology
        OntologyLoader ontologyLoader = new OntologyLoader();
        SimModel simModel = new SimModel(ontologyLoader.load());
        //process simulation ontology 
        List<IndividualSample> samples = simModel.processOntology();
        //get sequencial depedencies
        Set<String> dependencies = simModel.getDependencies(samples);

        for (int i = 1; i <= SIMULATIONS_NUMBER; i++) {
            logger.info("\n\t***** Simulation "+i+" *****\n");
            //make samples requests to get times
            Request request = new Request();
            samples = request.requestTimes(samples);
            //generate stats
            Statistic stats = new Statistic();
            logger.info("---- Times ----");
            samples = stats.generateTimes(samples);
            logger.info("---- Storage ----");
            samples = stats.generateStorage(samples);
            logger.info("---- Total ----");
            stats.printTotal(samples, dependencies);

            try {
                Thread.sleep(2 * 1000);
            } catch (InterruptedException ex) {
                logger.error(ex);  
            }
        }
        simModel.close();

    }

}
