/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ua.bioinformatics.disim.stats;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.apache.log4j.Logger;
import pt.ua.bioinformatics.disim.domain.IndividualSample;

/**
 *
 * @author sernadela
 */
public class Statistic {

    private static final Logger logger = Logger.getLogger(Statistic.class.getName());
    RandomGenerator rg;

    public Statistic() {
        rg = new RandomGenerator();
    }

    /**
     * Generate the estimate time for the requests
     *
     * @param file_samples
     * @return
     */
    public List<IndividualSample> generateTimes(List<IndividualSample> file_samples) {
        for (IndividualSample file_sample : file_samples) {

            long[] array = rg.generateGaussian(file_sample.getRequests(), file_sample.getDataTimes().values());
            DescriptiveStatistics stats = new DescriptiveStatistics();
            for (long value : array) {
                stats.addValue(value);
            }

            double milliseconds = stats.getSum();
            file_sample.setTotalTimesGenerated(milliseconds);

            logger.info(convertTimeToStr((long) milliseconds) + " to make " + array.length + " " + file_sample.getType() + " requests --> " + file_sample.getDescription());

        }
        return file_samples;
    }

    /**
     * Generate the estimate storage for the requests
     *
     * @param file_samples
     * @return
     */
    public List<IndividualSample> generateStorage(List<IndividualSample> file_samples) {
        for (IndividualSample file_sample : file_samples) {

            long[] array = rg.generateRandom(file_sample.getRequests(), file_sample.getStorage().values());
            DescriptiveStatistics stats = new DescriptiveStatistics();
            for (long value : array) {
                stats.addValue(value);
            }

            long storage = (long) Math.round(stats.getSum());
            file_sample.setTotalStorageGenerated(storage);

            logger.info(storage + " bytes are need to store " + array.length + " " + file_sample.getType() + " data --> " + file_sample.getDescription());

        }
        return file_samples;
    }

    /**
     * Prints total times and storage according to the dependencies
     *
     */
    public void printTotal(List<IndividualSample> file_samples, Set<String> dependencies) {
        double totalTimes = 0;
        double totalStorage = 0;
        for (IndividualSample file_sample : file_samples) {

            if (dependencies.contains(file_sample.getId())) {
                totalStorage = totalStorage + file_sample.getTotalStorageGenerated();
                totalTimes = totalTimes + file_sample.getTotalTimesGenerated();
            }

        }
        logger.info("Storage: " + (long) Math.round(totalStorage) + " bytes.\nTime: " + convertTimeToStr((long) totalTimes));

    }

    public String convertTimeToStr(long miliseconds) {
        String hms = String.format("%02dh %02dm %02ds", TimeUnit.MILLISECONDS.toHours(miliseconds),
                TimeUnit.MILLISECONDS.toMinutes(miliseconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(miliseconds)),
                TimeUnit.MILLISECONDS.toSeconds(miliseconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(miliseconds)));
        return hms;
    }

}
