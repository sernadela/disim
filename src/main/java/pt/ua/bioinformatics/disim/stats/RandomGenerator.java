/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ua.bioinformatics.disim.stats;

import java.util.Collection;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

/**
 *
 * @author Pedro Sernadela <sernadela at ua.pt>
 */
public class RandomGenerator {

    /**
     * Given a dataset generate random numbers according to a Gaussian
     * distribution
     *
     * @param testSize
     * @param dataset
     * @return
     */
    public long[] generateGaussian(int testSize, Collection<Long> dataset) {
        long[] numbers = new long[testSize];

        DescriptiveStatistics stats = new DescriptiveStatistics();
        for (long value : dataset) {
            stats.addValue(value);
        }

        RandomDataGenerator randomData = new RandomDataGenerator();

        for (int j = 0; j < testSize; j++) {
            double d = randomData.nextGaussian(stats.getMean(), stats.getStandardDeviation());
            long randomLong = (long) Math.round(d);
            if (randomLong >= 0) {
                numbers[j] = randomLong;
            } else {
                j--;
            }
        }

        return numbers;

    }

    long[] generateRandom(int requests, Collection<Long> values) {
        long[] numbers = new long[requests];
        
        DescriptiveStatistics stats = new DescriptiveStatistics();
        for (long value : values) {
            stats.addValue(value);
        }
        RandomDataGenerator randomData = new RandomDataGenerator();
        for (int i = 0; i < requests; i++) {
            long value = randomData.nextLong((long)Math.round(stats.getMin()-1.0), (long)Math.round(stats.getMax()+1.0));
            numbers[i]=value;
        }
        return numbers;
    }
}
