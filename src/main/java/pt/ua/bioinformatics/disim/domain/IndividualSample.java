/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pt.ua.bioinformatics.disim.domain;

import java.util.HashMap;

/**
 *
 * @author Pedro Sernadela <sernadela at ua.pt>
 */
public class IndividualSample {
    public static final String REPLACER="#replace#";
    private String id;
    private String description;
    private String type;
    private String connection;
    private int requests;
    private HashMap<String,Long> dataTimes;
    private HashMap<String,Long> storage;
    private String query;
    private double totalTimesGenerated;
    private double totalStorageGenerated;

    public IndividualSample(String id) {
        this.id=id;
        dataTimes=new HashMap<String, Long>();
        storage=new HashMap<String, Long>();
    }
    
    
    public double getTotalTimesGenerated() {
        return totalTimesGenerated;
    }

    public void setTotalTimesGenerated(double totalTimesGenerated) {
        this.totalTimesGenerated = totalTimesGenerated;
    }

    public double getTotalStorageGenerated() {
        return totalStorageGenerated;
    }

    public void setTotalStorageGenerated(double totalStorageGenerated) {
        this.totalStorageGenerated = totalStorageGenerated;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public void setStorage(HashMap<String, Long> storage) {
        this.storage = storage;
    }

    public HashMap<String, Long> getStorage() {
        return storage;
    }
    
    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setConnection(String source) {
        this.connection = source;
    }

    public void setRequests(int requests) {
        this.requests = requests;
    }

    public void setDataTimes(HashMap<String,Long> dataTimes) {
        this.dataTimes = dataTimes;
    }

    public String getDescription() {
        return description;
    }

    public String getType() {
        return type;
    }

    public String getConnection() {
        return connection;
    }

    public int getRequests() {
        return requests;
    }

    public HashMap<String,Long> getDataTimes() {
        return dataTimes;
    }
}
