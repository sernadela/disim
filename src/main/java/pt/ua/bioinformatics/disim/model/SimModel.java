/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ua.bioinformatics.disim.model;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;
import pt.ua.bioinformatics.disim.domain.IndividualSample;

/**
 *
 * @author sernadela
 */
public class SimModel {

    private static final Logger logger = Logger.getLogger(SimModel.class.getName());

    private OntModel model;

    public SimModel(OntModel model) {
        this.model = model;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(OntModel model) {
        this.model = model;
    }

    public Set<String> getDependencies(List<IndividualSample> samples) {

        Set<String> dependencies=new HashSet<String>();
        for (IndividualSample sample : samples) {

            //process all dependencies
            Property dependency = model.createObjectProperty(OntologyLoader.BASE + "isDependentOf");
            Resource individual=model.createOntResource(sample.getId());
            StmtIterator iter_dependency = model.listStatements(individual, dependency, (Resource) null);
            while (iter_dependency.hasNext()) {
                Statement node = iter_dependency.nextStatement();

                dependencies.add(node.getResource().toString());
                dependencies.add(individual.toString());
            }
        }
        return dependencies;

    }

    public List<IndividualSample> processOntology() {

        List<IndividualSample> samples = new ArrayList<IndividualSample>();

        ExtendedIterator<Individual> individuals = model.listIndividuals();
        //process all individuals
        while (individuals.hasNext()) {

            Individual individual = individuals.next();
            logger.debug( "\n" + individual);

            Property type = model.createOntProperty("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");

            //process all XML
            Resource xml = model.createResource(OntologyLoader.BASE + "XML");
            StmtIterator iter_xml = model.listStatements(individual.asResource(), type, xml);
            List<IndividualSample> xmlFileSamples = loadIndividuals(iter_xml.toList(), "XML");
            samples.addAll(xmlFileSamples);

            //process all JSON
            Resource json = model.createResource(OntologyLoader.BASE + "JSON");
            StmtIterator iter_json = model.listStatements(individual.asResource(), type, json);
            List<IndividualSample> jsonFileSamples = loadIndividuals(iter_json.toList(), "JSON");
            samples.addAll(jsonFileSamples);

            //process all CSV
            Resource csv = model.createResource(OntologyLoader.BASE + "CSV");
            StmtIterator iter_csv = model.listStatements(individual.asResource(), type, csv);
            List<IndividualSample> csvFileSamples = loadIndividuals(iter_csv.toList(), "CSV");
            samples.addAll(csvFileSamples);

            //process all SQL
            Resource sql = model.createResource(OntologyLoader.BASE + "SQL");
            StmtIterator iter_sql = model.listStatements(individual.asResource(), type, sql);
            List<IndividualSample> sqlSamples = loadIndividuals(iter_sql.toList(), "SQL");
            samples.addAll(sqlSamples);

        }

        return samples;
    }

    public void close() {
        model.close();
    }

    private List<IndividualSample> loadIndividuals(List<Statement> stats, String type) {

        Property source = model.createOntProperty(OntologyLoader.BASE + "connection");
        Property sample = model.createOntProperty(OntologyLoader.BASE + "sample");
        Property request = model.createOntProperty(OntologyLoader.BASE + "request");
        Property query = model.createOntProperty(OntologyLoader.BASE + "query");
        Property description = model.createOntProperty("http://purl.org/dc/elements/1.1/title");

        List<IndividualSample> samples = new ArrayList<IndividualSample>();

        for (Statement stmt : stats) {
            Resource subject = stmt.getSubject();// get the subject
            IndividualSample individualSample = new IndividualSample(subject.toString());

            individualSample.setType(type);

            //add description
            StmtIterator iter_description = model.listStatements(subject, description, (RDFNode) null);
            if (iter_description.hasNext()) {
                RDFNode node = iter_description.nextStatement().getObject();
                logger.debug("description: " + node.asNode().getLiteralValue().toString());
                individualSample.setDescription(node.asNode().getLiteralValue().toString());
            }

            //add source
            StmtIterator iter_source = model.listStatements(subject, source, (RDFNode) null);
            if (iter_source.hasNext()) {
                RDFNode node = iter_source.nextStatement().getObject();
                logger.debug("connection: " + node.asNode().getLiteralValue().toString());
                individualSample.setConnection(node.asNode().getLiteralValue().toString());
            }

            //add request
            StmtIterator iter_request = model.listStatements(subject, request, (RDFNode) null);
            if (iter_request.hasNext()) {
                RDFNode node = iter_request.nextStatement().getObject();
                logger.debug("request: " + node.asNode().getLiteralValue().toString());
                individualSample.setRequests(Integer.parseInt(node.asNode().getLiteralValue().toString()));
            }

            //add data samples
            StmtIterator iter_samples = model.listStatements(subject, sample, (RDFNode) null);
            while (iter_samples.hasNext()) {
                Statement stmt_sample = iter_samples.nextStatement();
                RDFNode s = stmt_sample.getObject();
                logger.debug("sample: " + s.asNode().getLiteralValue().toString());
                individualSample.getDataTimes().put(s.asNode().getLiteralValue().toString(), new Long(0));
            }

            if (type.equalsIgnoreCase("SQL")) {
                //add data samples
                StmtIterator iter_query = model.listStatements(subject, query, (RDFNode) null);
                if (iter_query.hasNext()) {
                    RDFNode node = iter_query.nextStatement().getObject();
                    logger.debug("query: " + node.asNode().getLiteralValue().toString());
                    individualSample.setQuery(node.asNode().getLiteralValue().toString());
                }
            }

            samples.add(individualSample);

        }

        return samples;

    }

}
