/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ua.bioinformatics.disim.model;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.util.FileManager;
import java.io.InputStream;

/**
 *
 * @author Pedro Sernadela <sernadela at ua.pt>
 */
public class OntologyLoader {

    static final String LOCATION = OntologyLoader.class.getResource("/").getPath() + "disim.owl";
    static final String BASE = "http://bioinformatics.ua.pt/disim/resource/";

    public static OntModel load() {
        // create an empty model
        OntModel model = ModelFactory.createOntologyModel();

        // use the FileManager to find the input file
        InputStream in = FileManager.get().open(LOCATION);
        if (in == null) {
            throw new IllegalArgumentException(
                    "File: " + LOCATION + " not found");
        }

        // read the RDF/XML file
        model.read(in, BASE);

        // write it to standard out
        //model.write(System.out);
        return model;

    }

}
